import React, { useState, useRef, useEffect } from "react";
import classes from "./NewProductModal.module.scss";
import { useHistory } from "react-router-dom";
import * as actions from "../../store/actions/index";
import axios from "axios";
import { amplitude } from "../../utilities/amplitude";
import { useSelector, useDispatch } from "react-redux";
import templates from "../../static_data/templates";
import nameInputValidationMsg from "../../utilities/nameInputValidationMsg";
import textareaValidationMsg from "../../utilities/textareaValidationMsg";

const NewProductModal = (props) => {
  const [inputValue, setInputValue] = useState("");
  const [nameRequired, setNameRequired] = useState("*Required.");
  const [descValue, setDescValue] = useState("");
  const [descRequired, setDescRequired] = useState("");
  const [isEditing, setIsEditing] = useState(false);

  const products = useSelector((state) => state.products.products);
  const newHashId = useSelector((state) => state.products.newProductId);
  const dispatch = useDispatch();

  const inputRef = useRef(null);
  let history = useHistory();

  useEffect(() => {
    setIsEditing(true);
    if (newHashId !== null) {
      history.push({
        pathname: "/home/products/" + newHashId,
        state: "new",
      });
    }
  }, [newHashId, history]);

  useEffect(() => {
    if (isEditing) {
      inputRef.current.focus();
    }
  }, [isEditing]);

  const inputChangeHandler = (event) => {
    let msg = "";
    let copyInput = {
      ...inputValue,
    };
    let newInput = event.target.value;
    copyInput = newInput;
    msg = nameInputValidationMsg(copyInput, products);

    setInputValue(copyInput);
    setNameRequired(msg);
  };

  const textareaChangeHandler = (event) => {
    let msg = "";
    let copyInput = {
      ...inputValue,
    };
    let newInput = event.target.value;
    copyInput = newInput;
    msg = textareaValidationMsg(copyInput);

    setDescValue(copyInput);
    setDescRequired(msg);
  };

  const saveNewProductHandler = (e) => {
    e.preventDefault();
    let organizationId = localStorage.getItem("organizationId");
    let fqdn = localStorage.getItem("fqdn");
    let name = inputValue;
    let desc = descValue;
    let obj = {
      organization_id: organizationId,
      user_id: null,
      name: name,
      description: desc,
      templates,
    };
    axios({
      method: "post",
      url: "https://" + fqdn + "/api/products/save",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        Accept: "application/json",
      },
      data: obj,
    })
      .then((response) => {
        localStorage.setItem("productId", response.data.object.hashid);
        dispatch(
          actions.addProductData(
            response.data.object,
            response.data.object.hashid
          )
        );
      })
      .catch((error) => {
        console.log(error.response);
        // handled by axios.interceptors
      });
  };
  const clickToBttn = (val) => {
    amplitude.getInstance().logEvent(val);
  };

  return (
    <div
      className={classes.NewProductContainer}
      onMouseDown={(e) => e.stopPropagation()}
      data-testid="NewProductContainer"
    >
      <div className={classes.NewProductHead}>
        Ready to bring a new product to the world?
      </div>
      <div className={classes.NewProduct}>
        <form onSubmit={(e) => saveNewProductHandler(e)}>
          <label>
            Name this product
            <input
              data-testid="name"
              ref={inputRef}
              type="text"
              placeholder="e.g. ProdGoal"
              onChange={(event) => inputChangeHandler(event)}
            />
            <p className={classes.Required} data-testid="nameRequired">
              {nameRequired}
            </p>
          </label>
          <label>
            Add description:
            <textarea
              data-testid="description"
              placeholder="e.g. Software development tool that makes productivity easy!"
              onChange={(event) => textareaChangeHandler(event)}
            />
            <p className={classes.Required} data-testid="descRequired">
              {descRequired}
            </p>
          </label>
          <div className={classes.SaveProductContainer}>
            <button
              data-testid="returnBttn"
              onClick={(e) => {
                props.closeModal(e);
                clickToBttn("NewProductClickToCancel");
              }}
            >
              Cancel and go back
            </button>
            <button
              data-testid="submitBttn"
              type="submit"
              disabled={
                nameRequired === "" && descRequired === "" ? false : true
              }
            >
              Create this product
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default NewProductModal;
