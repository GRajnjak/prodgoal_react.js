import React from "react";
import { screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Router } from "react-router-dom";
import { renderWithProviders } from "../utilities/test-utils";
import { createMemoryHistory } from "history";
import TemplateModal from "../components/Modals/TemplateModal/TemplateModal";
import templates from "../static_data/templates";

const initialProducts = {
  product: {
    id: 1,
    hashid: "testHash",
    name: "test",
    options: {
      templates: templates,
    },
  },
  templateSubmit: "Submit",
};
const history = createMemoryHistory();

describe("TemplateModal", () => {
  let closeModal;
  let onSetOptions;
  let submitTemplates;
  beforeEach(() => {
    closeModal = jest.fn();

    renderWithProviders(
      <Router history={history}>
        <TemplateModal
          templates={templates}
          product={initialProducts.product}
          closeModal={closeModal}
          templateSubmit={initialProducts.templateSubmit}
          onSetOptions={onSetOptions}
          submitTemplates={submitTemplates}
        />
      </Router>,
      {
        preloadedState: {
          products: initialProducts,
        },
      }
    );
  });

  test("if component renders properly", () => {
    const element = screen.getByTestId("template-modal");

    expect(element).toBeInTheDocument();
  });
  test("if child components renders properly", () => {
    const arrayOfElements = screen.getAllByTestId("template");

    expect(arrayOfElements).toHaveLength(7);
  });
  test("if child component renders it has checked board input by default", () => {
    const checkedElement = screen.getAllByRole("checkbox")[1];

    expect(checkedElement.checked).toEqual(true);
  });

  test("switching on template in child component ", () => {
    const switchElement = screen.getAllByTestId("switch")[2];
    const checkedElement = screen.getAllByRole("checkbox")[2];

    fireEvent.click(switchElement);

    expect(checkedElement.checked).toEqual(true);
  });
  test("switching off template in child component ", () => {
    const switchElement = screen.getAllByTestId("switch")[1];
    const checkedElement = screen.getAllByRole("checkbox")[1];

    fireEvent.click(switchElement);

    expect(checkedElement.checked).toEqual(false);
  });

  test("submitting templates (different than original) calls api and change button test", async () => {
    const switchElement = screen.getAllByTestId("switch")[5];
    let submitBttn = screen.getByTestId("submitBttn");

    fireEvent.click(switchElement);
    fireEvent.click(submitBttn);

    await waitFor(() => {
      expect(screen.getByText("Submitted")).toBeInTheDocument();
    });
  });
});
