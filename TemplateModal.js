import React, { Component } from "react";
import classes from "./TemplateModal.module.scss";
import { connect } from "react-redux";
import * as actions from "../../../store/actions/index";

import Template from "./Template";

class TemplateModal extends Component {
  state = {
    checkedTemplates: [],
    test: false,
  };

  componentDidMount() {
    let chosenTemplates = [];
    this.props.templates.map((el) => {
      if (el.active) {
        return chosenTemplates.push(el.blueprint);
      }
      return chosenTemplates;
    });
    this.setState({ checkedTemplates: chosenTemplates });
  }

  chooseTemplate = (blueprint) => {
    let currentTemplates = [...this.state.checkedTemplates];
    if (currentTemplates.includes(blueprint)) {
      const index = currentTemplates.indexOf(blueprint);
      currentTemplates.splice(index, 1);
    } else {
      currentTemplates.push(blueprint);
    }
    this.setState({ checkedTemplates: currentTemplates });
  };

  submitTemplates = () => {
    const organizationId = localStorage.getItem("organizationId");
    const productId = localStorage.getItem("productId");
    const productName = this.props.name;
    let templates = JSON.parse(JSON.stringify(this.props.templates));

    templates.map((t) => {
      if (this.state.checkedTemplates.includes(t.blueprint)) {
        t.active = true;
      } else {
        t.active = false;
      }
      return t;
    });

    let obj = {
      organization_id: organizationId,
      id: productId,
      name: productName,
      templates,
    };

    if (
      JSON.stringify(this.props.templates) === JSON.stringify(obj.templates)
    ) {
      this.props.closeModal();
    } else {
      this.props.onSetOptions(obj);
    }
  };

  render() {
    const template = (el) => (
      <Template
        key={el.blueprint}
        blueprint={el.blueprint}
        imgName={el.imgName}
        name={el.name}
        description={el.description}
        more={el.more}
        switch={true}
        chooseTemplate={this.chooseTemplate}
        checked={
          this.state.checkedTemplates.includes(el.blueprint) ? true : false
        }
      />
    );

    return (
      <div
        data-testid="template-modal"
        className={classes.TemplateModalContainer}
        onClick={(e) => e.stopPropagation()}
        onMouseDown={(e) => e.stopPropagation()}
      >
        <h2 className={classes.TemplateModalHead}>
          What do you need for this product?
        </h2>
        <div className={classes.TemplateModal}>
          <div>
            <div className={` ${classes.Section} ${classes.Delivery} `}>
              <h2>Delivery</h2>
              <ul>
                {this.props.templates.map((el) =>
                  el.section === "delivery" ? template(el) : null
                )}
              </ul>
            </div>
            <div className={` ${classes.Section} ${classes.Strategy} `}>
              <h2>Strategy</h2>
              <ul>
                {this.props.templates.map((el) =>
                  el.section === "strategy" ? template(el) : null
                )}
              </ul>
            </div>
          </div>
        </div>
        <div className={classes.SubmitTemplates}>
          <button
            data-testid="submitBttn"
            className={classes.SignBttn}
            onClick={this.submitTemplates}
          >
            {this.props.isTemplateSubmited ? "Submitted" : "Submit"}
          </button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    isTemplateSubmited: state.products.isTemplateSubmited,
  };
};
export const mapDispatchToProps = (dispatch) => {
  return {
    onSetOptions: (obj) => dispatch(actions.fetchTemplateChange(obj)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TemplateModal);
