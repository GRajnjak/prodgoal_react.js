import React from "react";
import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { renderWithProviders } from "../utilities/test-utils";

import NewProductModal from "../components/Modals/NewProductModal";

const history = createMemoryHistory();

const initialProducts = {
  products: [
    {
      hashid: "test1",
      id: 1,
      name: "test1",
    },
    {
      hashid: "test2",
      id: 2,
      name: "test2",
    },
  ],
  newProductId: "test3",
};

describe("NewProductModal", () => {
  beforeEach(() => {
    renderWithProviders(
      <Router history={history}>
        <NewProductModal />
      </Router>,
      {
        preloadedState: {
          products: initialProducts,
        },
      }
    );
  });

  test("NewProductModal is rendered properly and input has focus", () => {
    let element = screen.getByTestId("NewProductContainer");
    let name = screen.getByTestId("name");
    expect(element).toBeInTheDocument();
    expect(name).toHaveFocus();
  });
  test("if newProductId is not null rout will change to /home/products/newProductId", () => {
    expect(history.location.pathname).toBe("/home/products/test3");
  });

  test("if empty name is required and submit bttn disabled", () => {
    const name = screen.getByTestId("name");
    const nameRequired = screen.getByTestId("nameRequired");
    const submitBttn = screen.getByTestId("submitBttn");

    fireEvent.change(name, { target: { value: "" } });

    expect(nameRequired.textContent).toBe("*Required.");
    expect(submitBttn).toBeDisabled();
  });

  test('if name is long more than 191 character "Name is to long" msg is shown and submit bttn disabled', () => {
    const name = screen.getByTestId("name");
    const nameRequired = screen.getByTestId("nameRequired");
    const submitBttn = screen.getByTestId("submitBttn");

    fireEvent.change(name, {
      target: {
        value:
          "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",
      },
    });

    expect(nameRequired.textContent).toBe("Name is too long.");
    expect(submitBttn).toBeDisabled();
  });
  test("if name alreaday exists and submit bttn is disabled", () => {
    const name = screen.getByTestId("name");
    const nameRequired = screen.getByTestId("nameRequired");
    const submitBttn = screen.getByTestId("submitBttn");

    fireEvent.change(name, { target: { value: "test1" } });

    expect(nameRequired.textContent).toBe("Name must be unique.");
    expect(submitBttn).toBeDisabled();
  });
  test('if description is long more than 191 character "Description is to long" msg is shown and submit bttn disabled', () => {
    const descRequired = screen.getByTestId("descRequired");
    const description = screen.getByTestId("description");
    const submitBttn = screen.getByTestId("submitBttn");

    fireEvent.change(description, {
      target: {
        value:
          "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890",
      },
    });
    expect(descRequired.textContent).toBe("Description is too long.");
    expect(submitBttn).toBeDisabled();
  });
  test("if no msg-s are displayed and submit bttn enabled", () => {
    const name = screen.getByTestId("name");
    const nameRequired = screen.getByTestId("nameRequired");
    const descRequired = screen.getByTestId("descRequired");
    const description = screen.getByTestId("description");
    const submitBttn = screen.getByTestId("submitBttn");

    fireEvent.change(name, { target: { value: "123" } });
    fireEvent.change(description, { target: { value: "123" } });

    expect(nameRequired.textContent).toBe("");
    expect(descRequired.textContent).toBe("");
    expect(submitBttn).not.toBeDisabled();
  });

  test("submiting new product returns new hashid and redirects to that product", async () => {
    const name = screen.getByTestId("name");
    const description = screen.getByTestId("description");
    const submitBttn = screen.getByTestId("submitBttn");

    fireEvent.change(name, { target: { value: "123" } });
    fireEvent.change(description, { target: { value: "123" } });
    fireEvent.click(submitBttn);

    await waitFor(() => {
      expect(history.location.pathname).toBe("/home/products/test4");
    });
  });
});
